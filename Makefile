#
# Makefile for XeLaTeX
#
PROJECT=De_Bastonis_Regulis
MAIN_FILE=$(PROJECT).tex
OUTPUT=$(PROJECT).pdf

SOURCES=**.tex

FILES=$(SOURCES) Makefile 
FIG_DIR=
FIGURES=$(FIG_DIR:%=%/**)

DEBUG='true'
BIBER='false'
XELATEX_OPTS=-synctex=1 -interaction=nonstopmode --shell-escape

.PHONY : clean copy_sources copy_output

all: copy_sources
	make -C .build $(OUTPUT)
	make copy_output
	
copy_sources:
	rsync --verbose --checksum --recursive --mkpath --update --human-readable --progress --exclude=.git* --exclude=.build $(FILES) $(FIG_DIR) .build

copy_output:
	rsync --verbose --checksum --recursive --mkpath --update --human-readable --progress .build/$(OUTPUT) $(OUTPUT)


$(OUTPUT): $(FILES) $(FIGURES)
	@echo "Building $(PROJECT)"

ifeq ($(BIBER), 'true')
	OPTS="-bibtex"
else
	OPTS=""
endif

ifeq ($(DEBUG), 'true')
	latexmk $(OPTS) -pdfxe -pdfxelatex="xelatex --shell-escape %O %S" $(PROJECT)
else
	latexmk $(OPTS) -pdfxe -pdfxelatex="xelatex $(XELATEX_OPTS) %O %S" $(PROJECT) > /dev/null
endif


clean:
	@echo "I will clean up this mess ..."
	rm -rf .build

